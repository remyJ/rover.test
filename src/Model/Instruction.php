<?php

namespace App\Model;

use App\Interfaces\iInstruction;

class Instruction implements iInstruction
{

    
    private $currentInstruction;
    
    public function __construct($initial)
    {
        if(!$this->isValid($initial)){
            throw new InvalidArgumentException();
        }
        $this->currentInstruction = $initial;
        
    }

    
    public function isValid($command): bool
    {
        return 
            strlen($command) === 1 && 
            !empty(
                preg_match('/['. self::ACTION_TURN_LEFT .','. self::ACTION_TURN_RIGHT .','. self::ACTION_MOVE .']/', $command)
            );
    }

    public function equal($string): bool
    {
        // TODO: Implement equal() method.
    }

    public function asString(): string
    {
        return $this->currentInstruction;
    }

}
