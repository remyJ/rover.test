<?php

namespace App\Model;


use App\Interfaces\iHeading;

class Heading implements iHeading
{
    
    const CLOCKWISE_DIRECTION = [
        self::HEADING_NORTH => self::HEADING_EAST,
        self::HEADING_EAST => self::HEADING_SOUTH,
        self::HEADING_SOUTH => self::HEADING_WEST,
        self::HEADING_WEST => self::HEADING_NORTH
    ];
    const ANTICLOCKWISE_DIRECTION = [
        self::HEADING_NORTH => self::HEADING_WEST,
        self::HEADING_WEST => self::HEADING_SOUTH,
        self::HEADING_SOUTH => self::HEADING_EAST,
        self::HEADING_EAST => self::HEADING_NORTH
    ];
    
    
    private $currentDirection;

    public function __construct($initial)
    {

        if(!$this->isValidDirection($initial)){
           throw new InvalidArgumentException();
        }
        
        $this->currentDirection = $initial;
    }

    private function isValidDirection($direction){
        return 
            strlen($direction) === 1 && 
            !empty(
                preg_match(
                '/['. self::HEADING_NORTH .','. self::HEADING_EAST .','. self::HEADING_SOUTH .','. self::HEADING_WEST .']/', $direction)
            );
    }

    public function rotateLeft()
    {
        $this->currentDirection = self::ANTICLOCKWISE_DIRECTION[$this->currentDirection];
    }

    public function rotateRight()
    {
        $this->currentDirection = self::CLOCKWISE_DIRECTION[$this->currentDirection];
    }

    public function asString(): string
    {
        return $this->currentDirection;
    }
}
