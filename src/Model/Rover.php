<?php

namespace App\Model;


use App\Interfaces\iHeading;
use App\Interfaces\iInstruction;
use App\Interfaces\iPlateau;
use App\Interfaces\iPosition;
use App\Interfaces\iRover;
use App\Model\Instruction;
use App\Model\Position;

class Rover implements iRover
{
    
    private $plateau;
    private $position;
    private $heading;
    private $debug;
    
    public function __construct(iPlateau $plateau, iPosition $position, iHeading $heading)
    {
        $this->plateau = $plateau;
        $this->position = $position;
        $this->heading = $heading;
        $debug = false;
    }
    
    public function setDebug($value){
        $this->debug = $value;
    }
    

    public function getPosition(): iPosition
    {
        return $this->position;
    }

    public function getHeading(): iHeading
    {
        return $this->heading;
    }
    
    private function getPlateau(): iPlateau
    {
        return $this->plateau;
    }

    private function move(){
        $x = $this->getPosition()->getX();
        $y = $this->getPosition()->getY();

        switch($this->getHeading()->asString()){
            case Heading::HEADING_NORTH :
                $y++;
                break;
            case Heading::HEADING_EAST :
                $x++;
                break;
            case Heading::HEADING_SOUTH :
                $y--;
                break;
            case Heading::HEADING_WEST :
                $x--;
                break;
            default:

                break;
        }
        
        
        if($this->getPlateau()->isPositionValid(new Position($x,$y))){
            $this->getPosition()->setPosition($x,$y);
        }
    }


    public function action(iInstruction $instruction)
    {
        switch($instruction->asString()){
            case Instruction::ACTION_TURN_LEFT:
                $this->getHeading()->rotateLeft();
                break;
            case Instruction::ACTION_TURN_RIGHT:
                $this->getHeading()->rotateRight();
                break;
            case Instruction::ACTION_MOVE;
                $this->move();
                break;
            default:
                break;
        }
        
        $this->display();
    }
    
    private function isDebugMode(){
        return $this->debug;
    }
    
    private function display(){
        if(!$this->isDebugMode()){
            return false;
        }
        
        echo $this->getPosition()->asString();
    }
    
    

}
