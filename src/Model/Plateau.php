<?php

namespace App\Model;

use App\Interfaces\iPlateau;
use App\Interfaces\iPosition;


class Plateau implements iPlateau
{
    private $width;
    private $height;
    
    public function __construct($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function isPositionValid( iPosition $position):bool
    {
        return 
            $position->getX() <= $this->getWidth() && 
            $position->getY() <= $this->getHeight() &&
            $position->getX() >= 0 &&
            $position->getY() >= 0;
            
    }
}
