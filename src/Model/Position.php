<?php

namespace App\Model;

use App\Interfaces\iPosition;
use App\Model\Heading;

class Position implements iPosition
{
    private $x;
    private $y;
    
    public function __construct($x, $y)
    {

        
        $this->setPosition($x,$y);
    }
    
    private function isValidCoordonate($coordonate){
        return is_int($coordonate);
    }

    function setPosition($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    function getX()
    {
        return $this->x;
    }

    function getY()
    {
        return $this->y;
    }

    public function asString(){
        return 'x: '. $this->getX() .' y: '. $this->getY();
    }
}
