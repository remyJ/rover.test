<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\Heading;

final class HeadingTest extends TestCase
{

    public function testRotate(){
        $heading = new Heading('N');
        $heading->rotateRight();
        $this->assertEquals('E', $heading->asString());
        $heading->rotateRight();
        $this->assertEquals('S', $heading->asString());
        $heading->rotateRight();
        $this->assertEquals('W', $heading->asString());
        $heading->rotateRight();
        $this->assertEquals('N', $heading->asString());
        $heading->rotateLeft();
        $this->assertEquals('W', $heading->asString());
        $heading->rotateLeft();
        $this->assertEquals('S', $heading->asString());
        $heading->rotateLeft();
        $this->assertEquals('E', $heading->asString());
        $heading->rotateLeft();
        $this->assertEquals('N', $heading->asString());
    }
}
