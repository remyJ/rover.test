<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\Instruction;

final class InstructionTest extends TestCase
{

    public function testValidate(){
        $instruction = new Instruction(Instruction::ACTION_MOVE);
        $this->assertEquals('M', $instruction->asString());
    }
}
