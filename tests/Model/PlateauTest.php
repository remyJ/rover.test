<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\Plateau;
use \App\Model\Position;

final class PlateauTest extends TestCase
{

   public function testValidationCoordonate(){

       $plateau = new Plateau(5,5);
       $this->assertTrue($plateau->isPositionValid(new Position(0,0)));
       $this->assertTrue($plateau->isPositionValid(new Position(3,4)));
       $this->assertTrue($plateau->isPositionValid(new Position(4,4)));
       $this->assertTrue($plateau->isPositionValid(new Position(0,4)));
       $this->assertFalse($plateau->isPositionValid(new Position(5,6)));
       $this->assertFalse($plateau->isPositionValid(new Position((-1),6)));
   }
}
