<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\Rover;
use \App\Model\Heading;
use \App\Model\Plateau;
use \App\Model\Position;
use \App\Interfaces\iInstruction;
use \App\Model\Instruction;
use \App\Exceptions\HeadingException;
use \App\Exceptions\RoverPositionException;
use \App\Exceptions\RoverMoveException;

final class RoverTest extends TestCase
{
    public function testMove(){
        
        $rover = new Rover(new Plateau(5,5), new Position(0,0), new Heading(Heading::HEADING_NORTH));

        $rover->action(new Instruction(Instruction::ACTION_MOVE));
        $this->assertEquals(new Position(0,1), $rover->getPosition());

        $rover->action(new Instruction(Instruction::ACTION_TURN_RIGHT));
        $rover->action(new Instruction(Instruction::ACTION_MOVE));
        $this->assertEquals(new Position(1,1), $rover->getPosition());

        $this->assertEquals(Heading::HEADING_EAST, $rover->getHeading()->asString());
        
        $rover->action(new Instruction(Instruction::ACTION_TURN_RIGHT));
        $rover->action(new Instruction(Instruction::ACTION_TURN_RIGHT));
        $rover->action(new Instruction(Instruction::ACTION_MOVE));
        $rover->action(new Instruction(Instruction::ACTION_TURN_LEFT));
        $rover->action(new Instruction(Instruction::ACTION_MOVE));
        $this->assertEquals(new Position(0,0), $rover->getPosition());
        
        $rover->action(new Instruction(Instruction::ACTION_MOVE));
        $this->assertEquals(new Position(0,0), $rover->getPosition());
        
    }
    
    
    
}
