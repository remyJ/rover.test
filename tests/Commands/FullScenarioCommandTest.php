<?php

use App\Command\CreateUserCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class FullScenarioCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:run');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command' => $command->getName(),
            'file' => 'tests/robots.txt',
        ));

        $output = $commandTester->getDisplay();
        $this->assertContains('1 3 N', $output, 'output for rover1');
        $this->assertContains('5 1 E', $output, 'output for rover2');
    }

    public function testExecuteFileNotExists()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $this->expectException(\ErrorException::class);

        $command = $application->find('app:run');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command' => $command->getName(),
            'file' => 'tests/robotsNOtFound.txt',
        ));

    }
}
